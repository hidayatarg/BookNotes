// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAJA5Etvzs-KKj2UjP76qaf7sHxadBaPCQ",
    authDomain: "booknotes-4f339.firebaseapp.com",
    databaseURL: "https://booknotes-4f339.firebaseio.com",
    projectId: "booknotes-4f339",
    storageBucket: "booknotes-4f339.appspot.com",
    messagingSenderId: "725986470614"
  }
  

};
