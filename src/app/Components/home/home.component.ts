import { Component, OnInit } from '@angular/core';

//Firebase
import { FirebaseService } from './../../Services/firebase.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  favoriteBooks: any;
  unreadBooks: any;
  constructor(private firebaseService: FirebaseService) { }

  ngOnInit() {
    this.firebaseService.getFavoriteBooks().subscribe(favBooks => {
      this.favoriteBooks = favBooks;
      console.log('TopRated Books', this.favoriteBooks);
    })

    this.firebaseService.getUnreadBooks().subscribe(uBook => {
      this.unreadBooks = uBook;
      console.log('Unread Books', this.unreadBooks);
    })
  }

}
