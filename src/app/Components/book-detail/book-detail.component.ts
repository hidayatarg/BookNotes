
//Here we will show the book Details

import { Component, OnInit } from '@angular/core';

//Firebase service
import { FirebaseService } from './../../Services/firebase.service';

//we will get id from the active route
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  id:any;
  title;
  author;
  dateadded;
  dateread;
  price;
  rate;
  description;
  imageUrl;
 



  constructor( private firebaseService:FirebaseService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit() {
    //before book detail we need id
    //Id of the book
    //id in params come from the route in app.module as ** book-detail/id
    this.id=this.route.snapshot.params['id'];
    //console.log(this.id);

    this.firebaseService.getBookDetail(this.id).subscribe(book=>{
     //Get as an objcet console.log('bookdetails'+book)
      //console.log('bookdetails' + JSON.stringify(book));
      this.title=book.title;
      this.author = book.author;
      this.dateadded = book.dateadded;
      this.dateread = book.dateread;
      this.price = book.price;
      this.rate = book.rate;
      this.description = book.description;
      this.imageUrl = book.imageUrl;
      
      
    })
  }

}
