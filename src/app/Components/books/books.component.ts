
import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './../../Services/firebase.service';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  //get all the books
  
  allBooks: any;
  constructor(private firebaseService:FirebaseService) { }

  ngOnInit() {
    this.firebaseService.getBooks().subscribe(books=>{
      this.allBooks=books;
    })
  }

}


/* import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  //get all the books
  books: FirebaseListObservable<any[]>
  allBooks: any;
  constructor(private db:AngularFireDatabase) {
    this.books = db.list('/books');
    this.books.subscribe(books => {
      this.allBooks = books;
      console.log(this.allBooks);
    })

   }

  ngOnInit() {
  }

}
 */