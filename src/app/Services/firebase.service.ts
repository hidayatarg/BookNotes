import { Injectable } from '@angular/core';

//Firebase
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

//For using observable
import { Observable } from 'rxjs/Observable';
//Using Map
import "rxjs/add/operator/map";
import 'rxjs/operator/filter';

@Injectable()
export class FirebaseService {

  //From firebase
books: FirebaseListObservable<any[]>;

//Observable: helps to manage ansychronous data
//Import RXJS libaray
favoriteBooks: Observable<any>;
unreadBooks: Observable<any>;
bookDetail: Observable<any>;

  
constructor( private db: AngularFireDatabase) { }

getBooks(){
  this.books=this.db.list('/books') as FirebaseListObservable<any[]>;
  return this.books;
}


  getFavoriteBooks() {
    this.favoriteBooks = this.db.list('/books').map(books => {
      const topRatedBooks = books.filter(item => item.rate > 4);
      return topRatedBooks;
    })
    return this.favoriteBooks;
  }

  getUnreadBooks() {
    this.unreadBooks = this.db.list('/books').map(books => {
      const ub = books.filter(item => item.dateread == null);
      return ub;
    })
    return this.unreadBooks;
  }

  getBookDetail(id){
    //We will get single object
    this.bookDetail=this.db.object('/books/'+id) as Observable<any>;
    return this.bookDetail;
  }
}
