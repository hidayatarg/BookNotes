
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

//Form Module 
import{FormsModule} from '@angular/forms'

//Angular Material
import {
  MdButtonModule, MdCheckboxModule,
  MdCardModule,
  MdGridListModule,
  MdInputModule,
  MdDatepickerModule,
  MdNativeDateModule,
  MdToolbarModule,
  MdProgressSpinnerModule,
  MdTabsModule,
  MdListModule,
  MdIconModule } from '@angular/material'

//Angular Fire2 For FireBase Configuration
import { AngularFireModule } from 'angularfire2';
//TO interact firebase Database
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { environment } from './../environments/environment';

//AngularService 
import { FirebaseService } from './Services/firebase.service';


//Routes
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { BooksComponent } from './Components/books/books.component';
import { BookDetailComponent } from './Components/book-detail/book-detail.component';
import { AddBookComponent } from './Components/add-book/add-book.component';
import { EditBookComponent } from './Components/edit-book/edit-book.component';
import { DeleteBookComponent } from './Components/delete-book/delete-book.component';
import { NavbarComponent } from './Components/navbar/navbar.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'books', component: BooksComponent },
   { path: 'book-detail/:id', component: BookDetailComponent },
   { path: 'add-book', component: AddBookComponent },
   { path: 'edit-book/:id', component: EditBookComponent },
   { path: 'delete-book/:id', component: EditBookComponent }]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BooksComponent,
    BookDetailComponent,
    AddBookComponent,
    EditBookComponent,
    DeleteBookComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule, MdButtonModule, MdCheckboxModule,
    MdCardModule,
    MdGridListModule,
    MdInputModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdToolbarModule,
    MdProgressSpinnerModule,
    MdTabsModule,
    MdListModule,
    MdIconModule,
    //Route from the array we created at the top
    RouterModule.forRoot(appRoutes),
    //for firebase AngularFire2
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, FormsModule,
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
